<?php

session_start();

class Database {
    private $host = "localhost";
    private $username = "root";
    private $password = "";
    private $dbname = "task2";
    private $conn;

    public function __construct() {
        $this->conn = mysqli_connect($this->host, $this->username, $this->password, $this->dbname);
        if (!$this->conn) {
            die("Connection failed: " . mysqli_connect_error());
        } else echo "Connection success \n\n";
    }

    //  GETTING ALL THE DATA AND DISPLAYING IT
    public function readAll($table) {
        $getDataQuery = "SELECT * FROM $table";
        $result = mysqli_query($this->conn, $getDataQuery);
        ?>
        <br>
        <h5>List Of Users</h5>
        <?php
        while ($row = mysqli_fetch_assoc($result)) {
            ?>
            <form method="post">
                <label for="id">Id: <?php echo $row['id'] ?></label>
                <input hidden type="number" id="id" name="id" value="<?php echo  $row['id'] ?>">
                <label for="name">Firstname:</label>
                <input type="text" id="firstname" name="firstname" value="<?php echo  $row['firstname'] ?>">
                <label for="name">Lastname:</label>
                <input type="text" id="lastname" name="lastname" value="<?php  echo $row['lastname'] ?>">
                <label for="email">Email:</label>   
                <input type="text" id="email" name="email" value="<?php echo $row['email'] ?>">

                <button type="submit" name="update">Update</button>
                <button type="submit" name="delete">Delete</button>
            </form>
            <?php
        }

        
        //    UPDATE A DATA IN DATABASE
        if(isset($_POST['update'])) {
            $id = $_POST['id'];
            $firstname = $_POST['firstname'];
            $lastname = $_POST['lastname'];
            $email = $_POST['email'];
            
            $updateQuery = "UPDATE user SET firstname='$firstname', lastname='$lastname', email='$email' WHERE id=$id";
            $resultData = mysqli_query($this->conn, $updateQuery);

            if($resultData) {
                header("Location: task2.php");
                echo "$updateQuery \n\n";
                echo "Data updated successfully.";
                exit();
            } else {
                echo "Error updating data: " . mysqli_error($this->conn);
            }
        }
        //    DELETE A DATA IN DATABASE
        if(isset($_POST['delete'])) {
            $id = $_POST['id'];
            
            $deleteQuery = "DELETE FROM $table WHERE id=$id";
            $resultData = mysqli_query($this->conn, $deleteQuery);

            if($resultData) {
                header("Location: task2.php");
                echo "$deleteQuery \n\n";
                echo "Data deleted successfully.";
                exit();
            } else {
                echo "Error deleting data: " . mysqli_error($this->conn);
            }
        }
    }

    //  CREATING A TABLE
    public function createTable($sql) {
        if ($this->conn->query($sql) === TRUE) {
            echo "Table created successfully";
        } else {
            echo "Error creating table: " . $this->conn->error;
        }
    }

    //  INSERT DUMMY DATA IN DATABASE
    public function insertData() {
            $sql = "INSERT INTO user (firstname, lastname, email)
            VALUES ('John', 'Doe', 'john@example.com');";
            $sql .= "INSERT INTO user (firstname, lastname, email)
            VALUES ('Mary', 'Moe', 'mary@example.com');";
            $sql .= "INSERT INTO user (firstname, lastname, email)
            VALUES ('Julie', 'Dooley', 'julie@example.com')";
            
            if ($this->conn->multi_query($sql) === TRUE) {
                echo "New record created successfully";
                exit;
            } else {
                echo "Error: " . $sql . "<br>" . mysqli_error($this->conn);
            }
    }

    public function createForm() {

        // INSERT NEW DATA TO DATABASE
        if(isset($_POST['submit'])) {
            $first_name = $_POST['firstname'];
            $last_name = $_POST['lastname'];
            $email = $_POST['email'];
            
            $insertQuery = "INSERT INTO user (firstname, lastname, email)
            VALUES ('$first_name', '$last_name', '$email')";

            if (mysqli_query($this->conn, $insertQuery)) {
                echo "New record created successfully";
                header('Location: task2.php');
                exit;
            } else {
                echo "Error: " . $insertQuery . "<br>" . mysqli_error($this->conn);
            }
        }

        ?>
        <h5>Add new users</h5>
        <form method="post">
            <input type="text" name="firstname" placeholder="Enter first name" required>
            <input type="text" name="lastname" placeholder="Enter last name" required>
            <input type="text" name="email" placeholder="Enter email" required>
            <input type="submit" name="submit" value="Submit"/>
        </form>

        <?php
    }

    public function __destruct() {
        mysqli_close($this->conn);
    }
}



//      END OF CLASS            ///

$tablename = "user";

$sql = "CREATE TABLE MyGuests (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
firstname VARCHAR(30) NOT NULL,
lastname VARCHAR(30) NOT NULL,
email VARCHAR(50),
reg_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
)";


$userdb = new Database();
// $userdb->createTable($sql);
// $userdb->insertData();
$userdb->createForm();
$userdb->readAll($tablename);